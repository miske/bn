import React from "react"
import {LoginForm} from "../components/login"
import { useQuery } from "@apollo/react-hooks"
import { gql } from "apollo-boost"

const Login = () => {
  
  return (
    <div>
      <h1>Login</h1>
      <p>Test login form</p>
      <LoginForm />
    </div>
  )
}

export default Login