import React from "react"
import { useQuery } from "@apollo/react-hooks"
import { gql } from "apollo-boost"

const Index = () => {
  
  // our query that defines the attributes we want to get.
  const JOBS_QUERY = gql`
    query {me {userName, roles}}
  `

  interface CurrentUser {
    me: UserDetails;
  }

  interface UserDetails {
    username: string;
    email: string;
    password: string;
  }
  // the hook that calls the query.
  const  {loading, data} = useQuery<CurrentUser>(JOBS_QUERY);
  if (data){
    console.log(data.me);
  }
  return (
    <div>
      <h1>SITE NAME</h1>
      <p>Real estate service?</p>
    </div>
  )
}

export default Index
