
import React from 'react'
import Head from 'next/head';
import withApollo from "next-with-apollo"
import ApolloClient from 'apollo-client'
import { InMemoryCache,ApolloLink,HttpLink } from "apollo-boost"
import cookie from 'cookie';
import { IncomingMessage } from 'http';

const getToken = (req?: IncomingMessage, options = {}) => {
  const cookies = cookie.parse(
    req?.headers?.cookie ?? document.cookie,
    options,
  );

  return cookies.token;
};

const httpLink = new HttpLink({ uri: 'http://localhost:82/graphql' });

const authLink = new ApolloLink((operation, forward) => {
  // Retrieve the authorization token from  cookie storage.
  const token = getToken()
  // Use the setContext method to set the HTTP headers.
  operation.setContext({
    headers: {
      authorization: token ? `Bearer ${token}` : ''
    }
  });

  // Call the next link in the middleware chain.
  return forward(operation);
});


export default withApollo(
  ({ initialState }) =>
    new ApolloClient({
      link: authLink.concat(httpLink),
      cache: new InMemoryCache().restore(initialState || {})
    })
)