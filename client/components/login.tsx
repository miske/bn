import React, { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import cookie from 'cookie';
import redirect from "../lib/redirect";
const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    login(email:$email, password:$password)
  }
`;

interface LoginDetails {
  email: string;
  password: string;
}

export function LoginForm() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [ login,_] = useMutation(LOGIN, {update: (proxy, mutationResult) => {
  const data = (mutationResult["data"])
  const token = data["login"];
  document.cookie = cookie.serialize('token', token, { maxAge: 30 * 24 * 60 * 60,});// 30 days
  redirect({},'/');
  
}});

  async function handleSubmit() {
    login({variables:{email,password}});
  }

  return (
    <div>
      <h3>Login</h3>
      <form onSubmit={e => {
          e.preventDefault();
          handleSubmit()
      }}>
        <p>
          <label>Email</label>
          <input
            name="email"
            onChange={e => setEmail(e.target.value)}
          />
        </p>
        <p>
          <label>Password</label>
          <input
            name="password"
            onChange={e => setPassword(e.target.value)}
          />
        </p>
        <button onClick={() => email && password }>
         Login
        </button>
      </form>
   
    </div>
  );
}