FROM mhart/alpine-node:11

LABEL maintainer="Vincent Composieux <vincent.composieux@gmail.com>"

RUN apk add --update \
    php7-fpm \
    php7-apcu \
    php7-ctype \
    php7-curl \
    php7-dom \
    php7-gd \
    php7-iconv \
    php7-imagick \
    php7-json \
    php7-intl \
    php7-mcrypt \
    php7-mbstring \
    php7-opcache \
    php7-openssl \
    php7-pdo \
    php7-pdo_mysql \
    php7-mysqli \
    php7-xml \
    php7-xmlwriter \
    php7-zlib \
    php7-phar \
    php7-tokenizer \
    php7-session \
    php7-simplexml \
    php7-zip \
    php7-fileinfo \
    php7-bcmath \
    make \
    curl \
    alpine-sdk \
    tar \
    wget


RUN curl --insecure https://getcomposer.org/composer.phar -o /usr/bin/composer && chmod +x /usr/bin/composer

RUN echo "date.timezone = Europe/Belgrade" >> /etc/php7/php.ini
RUN echo "memory_limit = 1024M" >> /etc/php7/php.ini
RUN echo "upload_max_filesize = 20M" >> /etc/php7/php.ini
RUN echo "post_max_size = 20M" >> /etc/php7/php.ini
RUN echo "max_execution_time = 60"  >> /etc/php7/php.ini

ADD symfony.ini /etc/php7/php-fpm.d/
ADD symfony.ini /etc/php7/cli/conf.d/

ADD symfony.pool.conf /etc/php7/php-fpm.d/

CMD ["php-fpm7", "-F"]

WORKDIR /var/www/symfony
EXPOSE 9000
