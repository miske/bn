<?php
namespace App\GraphqlController;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Right;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\FailWith;

class MyController
{
    /**
     * @Query
     * @Logged
     */
    public function hello(string $name): string
    {
        return 'Hello ' . $name;
    }
}