<?php
namespace App\GraphqlController;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TheCodingMachine\Graphqlite\Validator\ValidationFailedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use App\Entity\User;



class UserController extends AbstractController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var UserRepository
     */
    private $userRepository;
    private $jwtTokenManagerInterface;


    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        UserRepository $userRepository,
        JWTTokenManagerInterface $jwtTokenManagerInterface
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
        $this->jwtTokenManagerInterface = $jwtTokenManagerInterface;
    }


    /**
     * @Mutation
     */
    public function createUser(string $email, string $password): string
    {

        $entityManager = $this->getDoctrine()->getManager();
        $user = new User($email, $password);

        // Let's validate the user
        //$errors = $this->validator->validate($user);

        // Throw an appropriate GraphQL exception if validation errors are encountered
        //ValidationFailedException::throwException($errors);

        $encodedPassword = $this->passwordEncoder
            ->encodePassword($user, $password);

        $user->setPassword($encodedPassword);
        $user->setEmail($email);

        $entityManager->persist($user);
        $entityManager->flush();

            return "OK";
    }


    /**
     * @Mutation
     */
    public function login(string $email, string $password): string
    {
              // Let's validate the user
        //$errors = $this->validator->validate($user);

        // Throw an appropriate GraphQL exception if validation errors are encountered
        //ValidationFailedException::throwException($errors);

        $user = $this->userRepository->findUserByEmail($email);
        $token = $this->jwtTokenManagerInterface
        ->create($user);

            return $token;
    }
}